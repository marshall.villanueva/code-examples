<?php
$page = 'home';
include('_include/header.php');
?>
<header class="banner container">
  <div>
    <img src="/images/FlogoWhite.jpg" />
    <h1>Branding + Print</h1>
  </div>
</header>
<div class="loading">

<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 360 360">

<path id="Vector" class="path" d="M154.5,273.07A2.09,2.09,0,0,1,154,273a114.16,114.16,0,0,1-18.74-7.27,2,2,0,0,1-1.1-1.77V179a2,2,0,0,1,2-2h58.77l6.34-14.47H136.08a2,2,0,0,1-2-2v-60a2,2,0,0,1,1.1-1.78,115.14,115.14,0,0,1,97-2.19,2,2,0,0,1,1,2.6l-7.39,16.9a2,2,0,0,1-1.1,1,2,2,0,0,1-1.51,0,92.48,92.48,0,0,0-66.76-2.94v40.52h-3.94V112.75a2,2,0,0,1,1.31-1.86A96.45,96.45,0,0,1,223,112.71l5.81-13.29a111.29,111.29,0,0,0-90.76,2.36v56.85h66.15a2,2,0,0,1,1.64.89,2,2,0,0,1,.16,1.88L198,179.82a2,2,0,0,1-1.8,1.17h-58.1v81.72a108.68,108.68,0,0,0,14.48,5.72V184.94h3.94v86.15a2,2,0,0,1-.78,1.58A2,2,0,0,1,154.5,273.07Z" />

</svg>

</div>
<div class="flexslider container">
  <ul class="slides">
    <li>
      <img src="/images/JPEG/BhoomiLogo.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/Bhoomi.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/NACLogo.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/NAC.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/AxoSimLogo.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/AxoSim.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/Hope.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/Poster.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/DavyJones.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/NOBICsmall.jpg" />
    </li>
    <!-- <li>
      <img src="/images/JPEG/NOLA.jpg" />
    </li> -->
    <li>
      <img src="/images/JPEG/ERB.jpg" />
    </li>
    <li>
      <img src="/images/JPEG/Tulane.jpg" />
    </li>
    <!-- <li>
      <img src="/images/JPEG/BearIsland.jpg" />
    </li> -->
    <li>
      <img src="/images/JPEG/TrainLocally.jpg" />
    </li>
    <!-- <li>
      <img src="/images/JPEG/Tulane.jpg" />
    </li> -->
    <li>
      <img src="/images/JPEG/OldeTownsmall.jpg" />
    </li>
  </ul>
</div>
<div class="container">
  <a class="about" href="#about">ABOUT</a>
  <a class="contact" href="mailto:nolanc888@gmail.com">CONTACT</a>
</div>
<div class="about">
  <a class="close" href="#">X</a>
  <img src="/images/JPEG/aiga-neworleans-chrisfreeman_1000px.jpg" alt="Christopher Freeman" />
  <h2>Christopher Freeman</h2>
  <p style="margin-bottom: 8px;">Durham native with an MPH from LSU. Tons of biotech and start-up experience. Large clients include Tulane, UCLA, New Orleans BioInnovation Center, National WWII Museum, Louisiana Department of Health, City of New Orleans, STOK (San Francisco) and CREO (Raleigh).</p>
  <p>My wife, daughter, and doggo keep me on my toes, carpentry is my hobby, and I do epic backcountry treks. Oh, and Geaux Saints!</p>
</div>
<div class="container">
  <p>If you'd like to discuss my process, work, and how we can do awesome things together, let's grab a coffee or beer.</p>
</div>
<?php include('_include/footer.php'); ?>
