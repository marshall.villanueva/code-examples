(function ($) {

  // Resources page form code
  let mafs = $("#my-ajax-filter-search");
  let mafsForm = mafs.find("form");
  function run_ajax(e) {
    e.preventDefault();
    if (mafsForm.find("#search").val().length !== 0) {
      var search = mafsForm.find("#search").val();
    }
    if (mafsForm.find("#resources").val().length !== 0) {
      var resources = mafsForm.find("#resources").val();
    }
    let data = {
      action: "my_ajax_filter_search",
      search: search,
      resources: resources
    }
    $.ajax({
      url: ajax_url,
      data: data,
      success: function (response) {
        mafs.find("div").empty();
        if (response) {
          for (let i = 0; i < response.length; i++) {
            // let content = response[i].content;
            let html = "<figure>";
            html += "  <a href='" + response[i].link + "' title='" + response[i].title + "'>";
            html += "    <img src='" + response[i].thumbnail + "' alt='" + response[i].title + "' />";
            html += response[i].title;
            html += "    <span>" + response[i].resources + "</span>";
            html += "  </a>";
            html += "</figure>";
            mafs.find("div").append(html);
          }
        }
      },
      error: function (error) {
        mafs.find("div").empty();
        let html = "<p class='no-result'>No matching resources found. Try a different filter or search keyword.</p>";
        mafs.find("div").append(html);
      }
    });
  }
  mafsForm.submit(function (e) {
    run_ajax(e);
  });
  mafsForm.change(function (e) {
    run_ajax(e);
  });
  $('#reset').click(function (e) {
    let data = {
      action: "my_ajax_filter_search",
      search: undefined,
      resources: undefined
    }
    $.ajax({
      url: ajax_url,
      data: data,
      success: function (response) {
        mafs.find("div").empty();
        if (response) {
          for (let i = 0; i < response.length; i++) {
            // let content = response[i].content;
            let html = "<figure>";
            html += "  <a href='" + response[i].link + "' title='" + response[i].title + "'>";
            html += "    <img src='" + response[i].thumbnail + "' alt='" + response[i].title + "' />";
            html += response[i].title;
            html += "    <span>" + response[i].resources + "</span>";
            html += "  </a>";
            html += "</figure>";
            mafs.find("div").append(html);
          }
        }
      }
    });
  });

  // Changes the color of the homepage nav
  let lastScrollTop = 0, delta = 1;
  $(window).scroll(function () {
    let nowScrollTop = $(this).scrollTop();
    if (Math.abs(lastScrollTop - nowScrollTop) >= delta) {
      if (nowScrollTop > lastScrollTop && lastScrollTop >= 0 && nowScrollTop > 0) {
        // scroll down
        $(".header-inner").css("background-color", "#ffffff");
        $("header#site-header").css("background-color", "#ffffff");
      } else {
        if (nowScrollTop == 0) {
          // scroll up
          $(".header-inner").css("background-color", "rgba(255,255,255,0)");
          $("header#site-header").css("background-color", "rgba(255,255,255,0)");
        }
        if (navigator.userAgent.match(/Android/i) ||
          navigator.userAgent.match(/webOS/i) ||
          navigator.userAgent.match(/iPhone/i) ||
          navigator.userAgent.match(/iPad/i) ||
          navigator.userAgent.match(/iPod/i) ||
          navigator.userAgent.match(/BlackBerry/i) ||
          navigator.userAgent.match(/Windows Phone/i)) {
          if (nowScrollTop < 10) {
            $(".header-inner").css("background-color", "rgba(255,255,255,0)");
            $("header#site-header").css("background-color", "rgba(255,255,255,0)");
          }
        }
      }
      lastScrollTop = nowScrollTop;
    }
  });

  // Button in footer that scrolls to the top
  $('.to-the-top').on('click', function () {
    $('html, body').animate({
      scrollTop: 0,
    }, 100)
  });

  // Homepage slider
  let metaslider_423 = function ($) {
    $('.flexslider').flexslider({
      slideshowSpeed: 3000,
      animation: 'slide',
      controlNav: false,
      directionNav: true,
      pauseOnHover: true,
      direction: 'horizontal',
      reverse: false,
      animationSpeed: 600,
      prevText: "Previous",
      nextText: "Next",
      fadeFirstSlide: false,
      easing: "linear",
      slideshow: true,
      itemWidth: 300,
      itemMargin: 0,
      minItems: 1,
      maxItems: 1
    });
    $(document).trigger('metaslider/initialized', '.flexslider');
  };
  let timer_metaslider_423 = function () {
    let slider = !window.jQuery ? window.setTimeout(timer_metaslider_423, 100) : !jQuery.isReady ? window.setTimeout(timer_metaslider_423, 1) : metaslider_423(window.jQuery);
  };
  timer_metaslider_423();

  // This is for the weird behavior that the menu has
  if ($(this) == $('.primary-menu .sub-menu ul.sub-menu') && $(this).mouseover()) {
    // console.log('it visible');
    // console.log($(this));
    $(this).css('z-index', '9999');
  }
  // else {
  //   console.log('it NOT visible');
  // }
  $('.primary-menu .sub-menu ul.sub-menu li').each(function () {
    $(this).mouseover(function () {
      $(this).parent().css('z-index', '9999');
    }).mouseleave(function () {
      $(this).parent().css('z-index', '0');
    });
  });

})(jQuery);
